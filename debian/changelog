python-os-collect-config (13.1.0-5) unstable; urgency=medium

  * Fix syntax warning (Closes: #1086894).

 -- Thomas Goirand <zigo@debian.org>  Thu, 02 Jan 2025 13:33:53 +0100

python-os-collect-config (13.1.0-4) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090552).

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Dec 2024 09:27:57 +0100

python-os-collect-config (13.1.0-3) unstable; urgency=medium

  * Removed extraneous dependency on python3-mock (Closes: #1071713).

 -- Thomas Goirand <zigo@debian.org>  Sun, 02 Jun 2024 13:51:02 +0200

python-os-collect-config (13.1.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 04 Oct 2023 22:05:32 +0200

python-os-collect-config (13.1.0-1) experimental; urgency=medium

  * New upstream release.
  * Fix (b-)depends for this release.
  * Cleans better.

 -- Thomas Goirand <zigo@debian.org>  Wed, 30 Aug 2023 10:18:41 +0200

python-os-collect-config (12.0.0-2) unstable; urgency=medium

  * Add Rules-Requires-Root: no to avoid #1001961 (Closes: #1002439).

 -- Thomas Goirand <zigo@debian.org>  Thu, 23 Dec 2021 15:01:57 +0100

python-os-collect-config (12.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 19 Jan 2021 10:46:56 +0100

python-os-collect-config (11.0.1-1) unstable; urgency=medium

  [ Michal Arbet ]
  * New upstream version
  [ Thomas Goirand ]
  * Add dh_python3 --shebang=/usr/bin/python3 (Closes: #976762)

 -- Thomas Goirand <zigo@debian.org>  Mon, 07 Dec 2020 23:49:24 +0100

python-os-collect-config (11.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 12 Jun 2020 10:48:55 +0200

python-os-collect-config (10.4.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Add debian/python3-os-collect-config.install.

 -- Thomas Goirand <zigo@debian.org>  Wed, 23 Oct 2019 10:48:33 +0200

python-os-collect-config (10.4.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Running wrap-and-sort -bast.
  * Use debhelper-compat instead of debian/compat.
  * d/control: Use team+openstack@tracker.debian.org as maintainer.

  [ Thomas Goirand ]
  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 18 Sep 2019 09:54:22 +0200

python-os-collect-config (10.3.0-2) unstable; urgency=medium

  * Add Breaks+Replaces: python-os-collect-config (Closes: #931849).

 -- Thomas Goirand <zigo@debian.org>  Fri, 12 Jul 2019 14:46:17 +0200

python-os-collect-config (10.3.0-1) unstable; urgency=medium

  * New upstream release:
    - Works with newer oslo.config (Closes: #931214).
  * Ran wrap-and-sort -bast.
  * Removed Mehdi and Julien from uploaders.
  * Switch to debhelper 11.
  * Switch to Python 3.
  * Fixed dependencies for this release.
  * Point VCS URLs to Salsa.

 -- Thomas Goirand <zigo@debian.org>  Wed, 03 Jul 2019 22:15:59 +0200

python-os-collect-config (0.1.15-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.
  * Reviewed (build-)depends.
  * Rewrote testr calls.

 -- Thomas Goirand <zigo@debian.org>  Mon, 14 Apr 2014 23:44:47 +0800

python-os-collect-config (0.1.11-1) experimental; urgency=low

  * Initial release. (Closes: #732173)

 -- Thomas Goirand <zigo@debian.org>  Sun, 16 Mar 2014 16:14:56 +0800
